FROM jupyter/minimal-notebook
ARG APP_DIR=/home/jovyan/work/
WORKDIR ${APP_DIR}/

#RUN apt-get update && apt-get install --yes \
#  && apt-get clean

COPY requirements*.txt ${APP_DIR}/
COPY scripts/install_dependencies.sh ${APP_DIR}/scripts/install_dependencies.sh
RUN pip install --upgrade pip
RUN scripts/install_dependencies.sh -e development

COPY . ${APP_DIR}

