from sklearn.datasets import load_iris 
import numpy as np 
import pandas as pd 

class DataLoader():
  def load(self):
    iris_data = load_iris() 
    return pd.DataFrame( 
      data = np.c_[ 
        iris_data['data'], 
        iris_data['target'], 
      ], 
      columns = iris_data['feature_names'] + ['target'] 
    ) 

