import pandas as pd

class DataBalancer():
  def __init__(self, target_name, data):
    self.target_name = target_name
    self.entire_data = data
    
  def balance(self):
    return self.entire_data

