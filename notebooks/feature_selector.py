import pandas as pd

class FeatureSelector():
  
  list_of_unusable_features = [
  ]
  
  def __init__(self, target_name, data):
    self.target_name = target_name
    self.data = data
  
  def select(self):
    return(
      self.data[
        self._get_list_of_usable_features()
      ]
    )
  
  def _get_list_of_usable_features(self):
    self.list_of_all_features = self.data.columns
    return [ feature
      for feature in self.list_of_all_features
        if feature not in type(self).list_of_unusable_features
    ]

