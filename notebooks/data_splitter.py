from sklearn.model_selection import train_test_split

class DataSplitter():
  def __init__(self, target_name, data):
    self.target_name = target_name
    self.data = data
    
  def split(self):
    return train_test_split( \
      self.data,
      self.data[self.target_name].values,
      stratify=self.data[self.target_name].values,
      test_size=0.7,
      random_state=42
    )

