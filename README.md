# Basic K Nearest Neighbor
This project is the most basic way to implement K Nearest Neighbor on the Iris data set.

Prerequisites
-------------
1. install docker
1. install docker-compose
1. install git
1. clone repository: `git clone --recursive https://gitlab.com/zhao-li/basic-k-nearest-neighbor.git`

Getting Started
---------------
1. run bootstrap.sh: `./bootstrap.sh`
1. start service: `docker-compose up`
1. browse to : `http://localhost:8888/?token=hash`

The server logs will output something like this:

    To access the notebook, open this file in a browser:
        file:///home/jovyan/.local/share/jupyter/runtime/nbserver-7-open.html
    Or copy and paste one of these URLs:
        http://7e61d13f7b08:8888/?token=ca2170ebbd15e3c4af81a5bd315616ff33c61b944d8dd996
     or http://127.0.0.1:8888/?token=ca2170ebbd15e3c4af81a5bd315616ff33c61b944d8dd996

